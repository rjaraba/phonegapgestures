var app={
	inicio: function() {
		this.iniciaBotones();
		this.iniciaFastClick();
		this.iniciaHammer();
	},

	iniciaFastClick : function() {
		FastClick.attach(document.body);
	},
	
	iniciaBotones: function() {
		var botonClaro = document.querySelector('#claro');
		var botonOscuro = document.querySelector('#oscuro');
			
		botonClaro.addEventListener('click', this.ponloClaro, false);
		botonOscuro.addEventListener('click', this.ponloOscuro, false);
	},

	iniciaHammer: function() {
		var zona = document.getElementById('zona-gestos');
		var hammertime = new Hammer(zona);
		
		/**
		 Por defecto el hammer NO activa el pinch y el rotate. Hay que activarlos
		 explicitamente.
		**/
		hammertime.get('pinch').set({enable: true});
		hammertime.get('rotate').set({enable: true});
		
		/*hammertime.on('tap doubletap pan swipe press pinch rotate', function(ev) {
			document.querySelector('#info').innerHTML = ev.type+'!';
			if (ev.type == 'swipe') {
				zona.style.backgroundColor = 'red';
			} else {
				zona.style.backgroundColor = '#7fba67';
			}
		});*/

		/**
		  Si quitamos el evento 'pan' podremos ver el 'swipe' 
		**/
		/*hammertime.on('tap doubletap swipe press pinch rotate', function(ev) {
			document.querySelector('#info').innerHTML = ev.type+'!';
			if (ev.type == 'swipe') {
				zona.style.backgroundColor = 'red';
			} else {
				zona.style.backgroundColor = '#7fba67';
			}
		});

		/**
		   Idem para 'rotate'. Este evento solo funciona si desactivamos el 'pinch'
		 **/
		/*hammertime.on('tap doubletap swipe press rotate', function(ev) {
			document.querySelector('#info').innerHTML = ev.type+'!';
		});		  I*/

		zona.addEventListener ('webkitAnimationEnd', function(ev) {
			zona.className='';
		});

		hammertime.on ('tap', function(ev) {
			zona.className='tap';
		});

		hammertime.on ('doubletap', function(ev) {
			zona.className='doubletap';
		});

		hammertime.on ('press', function(ev) {
			zona.className='press';
		});

		hammertime.on ('swipe', function(ev) {
			var clase=undefined;
			direccion=ev.direction;

			if (direccion==4) clase='swipe-derecha';
			if (direccion==2) clase='swipe-izquierda';

			zona.className=clase;
		});

		hammertime.on ('rotate', function(ev) {
			zona.innerHTML = 'Hola';
			var umbral=25;
			if (ev.distance > umbral) zona.className='rotate';
		});
	},
	
	ponloClaro : function() {
		var zona = document.getElementById('zona-gestos');
		zona.className = 'claro';
	},
	
	ponloOscuro : function() {
		var zona = document.getElementById('zona-gestos');
		zona.className = 'oscuro';
	},
};

if ('addEventListener' in document) {
	document.addEventListener('DOMContentLoaded', function() {
		app.inicio();
	}, false);
}